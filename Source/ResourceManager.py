import os
import sys
import json

class ResourceManager:
    """Handle the static resources needed by the other classes.

    member variables:
    main_path -- str: path to the main folder.
    dicts -- dict: caches all dictonaries for translation that were already used.
    article -- dict: containing all details about the requestable articles.
    get_url -- url: to which to send requests.

    methods:
    translate() -- Translate between codes and human readable representations.
    process() -- processes the response.
    save_csv() -- Save request to csv.
    save_pkl() -- Save request to pkl.
    generate_filename() -- generates filename from request parameters.
    """

    def __init__(self):
        """initialize the resouce manager."""
        # TODO: find nice solution to create the main_path using sys.argv gives problem with external calls 
        self.main_path = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))#sys.argv[0]))
        self.dicts = {}
        self.article_dict = self.get_shared_resource("articles",".json")
        self.get_url = r"https://transparency.entsoe.eu/api"


    def translate(self, type:str, to:str, name):
        """Translate a code representation (or a list of representations) to a human readable one or the other way round.
        If the translation fails the original input is returned, thus translating a mixed list will result in a uniform one.

        keyword arguments:
        type -- str: the type of data you are trying to translate (e.g. "processType").
        to -- str: can be "code" if translating to code or "human" when translating to human readable format.
        name -- str|list: name/codes that are to be translated. Output type will be the same as this.
        """
        if(type in ("periodStart","periodEnd","securityToken")):
            # Add potential porcessing for additional input formats here
            return name
        if("domain" in type.lower()):
            type = "domain"
        type += "_to_" + to
        dict = self.get_shared_resource(type,".json")
        if isinstance(name, list):
            return [dict.get(n,n) for n in name]
        # returns the input if key not found in input, this way it leaves already machine readable code intact
        return dict.get(name,name)

    def translate_dict(self,to,d:dict):
        """Translate the values of a dictionary where the key specifies the type of data to code or human readable.
        If the translation fails the original input is returned, thus translating a mixed list will result in a uniform one.

        keyword arguments:
        to -- str: can be "code" if translating to code or "human" when translating to human readable format.
        d -- dict: dictionary where the key specifies the type of data the values belong to.
                If a type can not be found the values are just copied.
        """
        return {k:self.translate(k, to, v) for k,v in d.items()}
    
    def get_shared_resource(self,name,ending=''):
        """Loads a JSON file or loads it from the cache.
        
        keyword arguments:
        name -- str: name of the file.
        ending -- str: file ending of the resource including the ".". Not necessary when loading from cache only.
        """
        if name in self.dicts.keys():
            return self.dicts[name]
        with open(os.path.join(self.main_path,"shared_resources", name+ending),'r') as in_file:
            dict = json.load(in_file)
            self.dicts.update({name:dict})
        return dict


    def load_security_token(self,rel_path="securitytoken.txt"):
        """Loads and return the security token needed for API access.
        
        keyword arguments:
        rel_path -- str: path to the file containing the secuity token relative to the main folder.
        """
        try:
            with open(os.path.join(self.main_path,rel_path),'r') as in_file:
                return in_file.read().strip()
        except FileNotFoundError:
            print("File with security token 'securitytoken.txt' could not be open, please check if it exist in the correct location: "
                    , os.path.join(self.main_path,rel_path),file=sys.stderr)
            raise FileNotFoundError("File with security token 'securitytoken.txt' could not be open, \
                    please check if it exist in the correct location: ".join(os.path.join(self.main_path,rel_path)))