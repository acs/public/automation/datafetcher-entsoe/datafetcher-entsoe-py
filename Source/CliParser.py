"""Define a two step parsing for commandline input"""
import argparse
import sys
import os
import json
from dateutil import parser as date_parser
from .ResourceManager import ResourceManager


class CliParser:
    def __init__(self, res=ResourceManager(),args=sys.argv[1:]):
        self.res = res
        self.args = args
        self._basic_input = None
        self._full_input = None

    def get_basic_input(self):
        if self._basic_input is None:
            self._basic_input = self.parse_user_input_basic()
        return self._basic_input
    
    def get_full_input(self):
        if self._full_input is None:
            self._full_input = self.get_basic_input()
            self._full_input.update(self.parse_for_articles(basic_input_dict=self._full_input))
        return self._full_input

    def parse_user_input_basic(self,args=None):
        """parse the part that is needed for every request.
        
        That are -s: Starting time
                -e: Ending time
                -a: One or more articles
        """
        if args is None:
            args = self.args
        parser = argparse.ArgumentParser()
        parser.add_argument("-s","--periodStart", nargs=1, type=str, required=True, metavar="YYYYMMDDhhmm")
        parser.add_argument("-e","--periodEnd", nargs=1, type=str, required=True, metavar="YYYYMMDDhhmm")
        parser.add_argument("-a","--article", nargs='+', type=str, required=True)

        ret = vars(parser.parse_known_args(args=args)[0])
        if(date_parser.parse(ret["periodStart"][0]) > date_parser.parse(ret["periodEnd"][0])):
            raise AttributeError("periodStart must be AFTER periodEnd.")
        
        # remove duplicates from list
        for k,v in ret.items():
            ret[k] = list(dict.fromkeys(v))
        self._basic_input = ret
        return ret


    def parse_for_articles(self,basic_input_dict=None, args=None,):
        """Parses the remaining parameters provided and checks them for completeness.
        
        All needed/optional parameters of requested articles must/can be provided. 
        They will be assoziated with the corresponding articles later.
        Every legal combination of articles and parameters will be requested,
        if an optional parameter is provided it will always be used where possible, 
        even when ommiting it would still be a legal request.
        """
        if basic_input_dict is None:
            basic_input_dict = self.get_basic_input()
        if args is None:
            args = self.args

        main_path = self.res.main_path
        parser = argparse.ArgumentParser()
        
        # # TODO this should now be handled by the ResourceManager
        # with open(os.path.join(main_path,"shared_resources", "articles.json"),'r') as in_file:
        #     article_dict = json.load(in_file)
        # with open(os.path.join(main_path,"shared_resources", "article_to_code.json"),'r') as in_file:
        #     name_dict = json.load(in_file)

        article_dict = self.res.get_shared_resource("articles",".json")
        name_dict = self.res.get_shared_resource("article_to_code",".json")
        # use sets here to avoid double input parsing
        required_inputs = set()
        allowed_inputs = set()

        for art in basic_input_dict['article']:
            required_inputs.update(article_dict[art]['needed_params'].keys())
            allowed_inputs.update(article_dict[art]['potential_params'].keys())

        for input in required_inputs:
            parser.add_argument("--"+input, nargs='+', required=True)

        for input in allowed_inputs:
            parser.add_argument("--"+input, nargs='+')

        # same dict but with all unused allowed_inputs and duplicates removed
        return {k:list(dict.fromkeys(v)) for k,v in vars(parser.parse_known_args(args)[0]).items() if v is not None}
    
