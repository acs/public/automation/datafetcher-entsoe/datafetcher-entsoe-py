import sys
import os
import requests
import logging
import datetime
from itertools import product
import xml.etree.ElementTree as ET
from dateutil import parser
import pandas as pd
from .ResourceManager import ResourceManager

class MyRequest:
    """Handle a single API request.

    member variables:
    art_code -- str: string representing what data is requested by article number.
    params -- dict: all parameters send to the API for the request. 
    resonse -- response: full respone of the API.
    data -- pandas.Dataframe: processed data recieved from the API.
    res -- ResourceManager: handle all loading and lookup of static resources.
    logger -- logging.Logger: handle logging

    methods:
    get() -- sends the request.
    process() -- processes the response.
    save_csv() -- Save request to csv.
    save_pkl() -- Save request to pkl.
    generate_filename -- generates filename from request parameters.
    """
    def __init__(self, art_code:str, params:dict, res=ResourceManager()):
        """Gather the data for the request and links the resourcemangager.

        Keyword arguments:
        art_code -- str: string representing what data is requested by article number.
        params -- dict: all parameters send to the API for the request.
        res -- ResourceManager: link to an instance of a resource manager to share it, defaults to creating a new manager.
        """

        self.art_code = art_code
        self.params = params
        self.response = None
        self.data = None
        self.res = res
        self.filename = self.generate_filename()
        
        log_filename = os.path.join(self.res.main_path,'logs','log.txt')
        self.logger = logging.getLogger(self.filename)
        handler = logging.FileHandler(filename = log_filename)
        handler.setFormatter(logging.Formatter('{levelname} | {name} | {message}',style='{'))
        self.logger.addHandler(handler)
        self.logger.setLevel(logging.DEBUG)
        
        # self.file_name = self.generate_filename()


    def get(self):
        """execute the defined request"""

        # print(f"requesting data with parameters: {self.params}")
        self.logger.info(f"requesting data with parameters: {self.params}")
        self.response = requests.get(self.res.get_url, params=self.res.translate_dict("code",self.params))
        if self.response.status_code != 200:
            self.logger.warning(f"request was denied with status code {self.response.status_code}")
            # print(f"request was denied with status code {self.response.status_code}", file=sys.stderr)

    def process(self):
        """process the raw response to an XMLTree and then to a pandas.Dataframe."""
        # with open('response.xml','w') as outfile:
        #     print(self.response.text, file=outfile)
        root = ET.fromstring(self.response.text)
        # This removes the namespace from the XML tree.
        # The namespace was added in front of every tag and made lookup of elements difficult for no reasson.
        # getting the namespace and adding it to the lookups later on would have been a different posibility
        for el in root.iter():
            el.tag = el.tag.rsplit('}')[-1]
        self.data = self.xml_tree_to_df(root) #, self.art_code, self.params['periodStart'],self.params['periodEnd'])


    def generate_filename(self):
        """Generate a filename from request parameters."""
        # TODO: fit to different input format, if input was human readable output should too
        
        ret = f"{self.art_code}_".replace(r".",r"-")
        for key in self.params.keys():
            if key.rsplit('_')[-1] == "Domain":
                ret += self.res.translate(key,"code", self.params[key]) + "_"

        ret += f"{self.params['periodStart']}-{self.params['periodEnd']}"
        
        return ret


    def xml_tree_to_df(self, root: ET.Element):
        """Transforms the parsed and cleaned XML response to a pandas dataframe.

        keyword arguments:
        root - xml.etree.ElementTree.Element: root of the XML-tree the is to be parsed.
        """
        all_series = root.findall('TimeSeries')
        request_start = pd.to_datetime(self.params["periodStart"], utc=True)
        request_end = pd.to_datetime(self.params["periodEnd"], utc=True)
        xml_interval = root.find('time_Period.timeInterval')
        if xml_interval:
            data_start = pd.to_datetime(xml_interval.find('start').text, utc=True)
            data_end = pd.to_datetime(xml_interval.find('end').text, utc=True)
        all_dfs = [] # the dataframes of each single timeseries are stored in this

        for i, series in enumerate(all_series):
            col_name = self.get_col_name(series)
            period = series.find("Period") # this is where the actual datapoints for the seres are
            new_values = [float(point.find('quantity').text) for point in period.findall('Point')]
            start = pd.to_datetime(period.find('timeInterval').find('start').text)
            end = pd.to_datetime(period.find('timeInterval').find('end').text)
            # print(type(end-start))
            timeseries = pd.date_range(start=start,end=end,periods=len(new_values)+1, closed='left')
            all_dfs.append(pd.DataFrame(new_values, index=timeseries, columns=[col_name]))
        try:
            combined_df = all_dfs[0].copy()
        except IndexError:
            self.logger.error("Response did not include timeseries")
            print("Response did not include timeseries", file=sys.stderr)
            with open("faulty_response.xml",'w') as outfile:
                print(self.response.text,file=outfile)
            return

        for df in all_dfs[1:]:
            # XXX I can't believe there is no nicer way to combine in multiple axis but this works
            # The gathered DFs are all single column and are guranteed to not overlap.
            # "combine_first" goes thorugh all combinations of dates and columns and fills the entry in the return Dataframe 
            # with the first non-NAN value it finds in those coordinates. Since there is no overlap this just combines the Dataframes into one.
            combined_df = combined_df.combine_first(df)
        
        if combined_df.shape[0] > 1: #frequency can only be estimated if there is more than one value
            dt = (combined_df.index[1:] - combined_df.index[:-1]).min() # doing this before combining is not possible.
            # this adds potentially missing rows with NANs since it renomalizes the index to be evenly spaced.
            combined_df = combined_df.asfreq(dt)
            # print(dt)
            self.logger.info("The frequency of values was estimated to be {dt}")
        else:
            self.logger.info("The frequency of values was not estimated, only 1 value was available.")
        # if there is data missing at the end or the start of the requested period there are currently no NANs added for those values.
        # TODO: Pad DF with NANs at the start and end if values are missing.
        # padded_nan_df = pd.DataFrame()
        # print(request_start,'-',request_end)
        if data_start and data_start-request_start > pd.Timedelta(0):
            if dt:
                padding_start_index = pd.date_range(start=request_start,end=data_start,freq=dt,closed='right')
                padding_start_df = pd.DataFrame(index=padding_start_index)
                combined_df = combined_df.combine_first(padding_start_df)
                # print(int((data_start-request_start)/dt), end=' ', file=sys.stderr)

            # print("Missing values at the start of requested period were omitted", file=sys.stderr)
            # print(f"requested start was {request_start}, data starts at {data_start}", file=sys.stderr)

        if data_end and request_end-data_end > pd.Timedelta(0):
            if dt:
                padding_end_index = pd.date_range(start=end,end=request_end,freq=dt,closed='left')
                padding_end_df = pd.DataFrame(index=padding_end_index)
                combined_df = combined_df.combine_first(padding_end_df)
                # print(int((request_end-data_end)/dt), end=' ', file=sys.stderr)
            # print("Missing values at the end of requested period were omitted", file=sys.stderr)
            # print(f"requested end was {request_end}, data ends at {data_end}", file=sys.stderr)
            

        combined_df.index.rename("Time", inplace=True)
        
        nan_df = combined_df.isnull() # shaped like the original DF but 0 for values and 1 for NaNs
        nan_rows = nan_df.sum(axis=1) # counts the number of Nans in each row
        nan_cols = nan_df.sum(axis=0) # counts the number of Nans in each column
        if nan_df.any(axis=None):
            # counts the number of rows/columns with atleast 1 NaN
            self.logger.info(f"The data contains a total of {nan_df.sum().sum()} NAN's "\
                            f"in {nan_rows[nan_rows>0].shape[0]} rows and "\
                            f"{nan_cols[nan_cols>0].shape[0]} columns.")
            # print(f"The data contains a total of {nan_df.sum().sum()} NaN's ", file=sys.stderr, end='')
            # print(f"in {nan_rows[nan_rows>0].shape[0]} rows and ", end='', file=sys.stderr)
            # print(f"{nan_cols[nan_cols>0].shape[0]} columns.", file=sys.stderr)
        else:
            self.logger.info(f"The data contains no NAN's.")
            
        return combined_df


    def get_col_name(self, series: ET.Element):
        """Get possible column names. This is necessary since the XML response does not directly state what it contains.

        keyword arguments:
        series -- xml.etree.ElementTree.Element: Timeseries to be search for a potential name

        Any timeseries in the xml response is a timeinterval (of potentially many) in a single column. It is not uniformly stated what data the series contains.
        Candidates for tags containing that information are the potential parameters you can send in a request, 
        which would select a single comlumn when specified in the request.
        As an example if you requested a table with a column for each energy type
        the XML-Timeseries would contain a tag like "<energyType>Type1</energyType>". 
        "energyType" would be a potential parameter and "Type1" a potential value for that parameter but also the column name. 
        """
        # Any key could be a potential tag containing the column name.
        key_set = self.res.article_dict[self.art_code]["potential_params"].keys()
        # TODO potential names are gathered here, not realy used atm since only first is chosen
        cols = []
        unit = series.find('quantity_Measure_Unit.name')
        # serach through the timeseries for the hint of the potential parameter.
        for key in key_set:
            # TODO should there be a "if code is None:" in this loop (seems like we got lucky(?) to always have the first key to be a hit)
            code = next(series.iter(key)).text
            col_name = self.res.translate(key, "human", code)
            
            if unit is not None:
                col_name += f" [{unit.text}]"
            cols.append(col_name)

        # if none of the candidates are found the original article name must have been the column name already (since then there is only one column).
        if not cols:
            # TODO: this should be done by the resource handler
            return self.res.article_dict[self.art_code]["name"] + f" [{unit.text}]"
        else:
            # TODO cols contains multiple possible names it should be compared to other series and then decided which one to choose
            return cols[0] 

    def save_csv(self,
                rel_path=os.path.join("output","csv"),
                file_name=None):
        """Save a csv representation of the requested data.

        keyword arguments:
        rel_path -- str: path to the output file relative to the main script. Defaults to "/output/csv/".
                        Will be created if not existend.
        file_name -- str: name of the output file. Defaults to generating one from the request parameters (see generate_filname()).
                        ".csv" will be appended autmomaticly
        """
        if self.data is None:
            self.logger.warn(f"No processed data available for {file_name}, try .process() first.")
            # print(f"No processed data available for {file_name}, try .process() first.", file=sys.stderr)
            return
        if file_name is None:
            file_name=self.generate_filename()
        path = os.path.join(self.res.main_path, rel_path)
        if not os.path.exists(path):
            os.makedirs(path)
        self.data.to_csv(os.path.join(path,f"{file_name}.csv"), na_rep='NAN')

    
    def save_pkl(self,
                rel_path=os.path.join("output","pkl"),
                file_name=None):
        """Save a pickle representation of the requested data.

        keyword arguments:
        rel_path -- str: path to the output file relative to the main script. Defaults to "/output/pkl/".
                        Will be created if not existend.
        file_name -- str: name of the output file. Defaults to generating one from the request parameters (see generate_filname()).
                        ".pkl" will be appended autmomaticly
        """
        if self.data is None:
            self.logger.warn(f"No processed data available for {file_name}, try .process() first.")
            # print(f"No processed data available for {file_name}, try .process() first.", file=sys.stderr)
            return
        if file_name is None:
            file_name=self.generate_filename()
        path = os.path.join(self.res.main_path, rel_path)
        if not os.path.exists(path):
            os.makedirs(path)
        self.data.to_pickle(os.path.join(path,f"{file_name}.pkl"))