import sys
from itertools import product

from .CliParser import CliParser
from .MyRequest import MyRequest
from .ResourceManager import ResourceManager

import logging

class Fetcher:
    """Handle the overall flow of the script call.

    member variables:
    res -- ResourceManager: handle all loading and lookup of static resources.
    parser -- CliParser: handle the user input. 
    sec_token -- str: personal security token of the user.
    requests -- list: singular requests generated from user input.
    
    methods:
    get() -- sends the requests.
    process() -- processes the responses.
    save_csv() -- Save all requests to csv.
    save_pkl() -- Save all requests to pkl.
    """

    def __init__(self, cli_params = sys.argv[1:]):
        """Loads necessary resources, parses user input and prepares corresponding requests.

        Keyword arguments:
        cli_params -- list: strings representing command line input, excluding program name. Defaults to the actual command line input.
        """
        # print(sys.argv[1:])
        self.res = ResourceManager()
        # self.user_input = CliParser.parse_user_input_basic()
        # self.user_input.update(CliParser.parse_for_articles(self.user_input))
        self.sec_token = self.res.load_security_token()
        self.parser = CliParser(res=self.res, args=cli_params)
        self.requests = self._prepare_requests()
        self.logger = logging.getLogger(__name__)



    def _prepare_requests(self):
        """Generate a list of all legal combinations of request parameters."""
        input = self.parser.get_full_input()
        ret = []
        for art in input["article"]:
            art_code = self.res.translate("article","code",art)
            article = self.res.article_dict[art_code]
            tmp = {"securityToken": self.sec_token}
            tmp.update(article["fixed_params"])
            for key in tmp.keys():
                tmp[key] = [tmp[key]]
            print(tmp)
            # Only add keys that are eligable for the requested article
            tmp.update({key:input[key] for key in input.keys()
                    if key in article["needed_params"].keys()
                    or key in article["potential_params"].keys()})
            # Start and end must be added specificcaly as they are not contained in "needed_params"
            tmp.update({"periodStart":input["periodStart"],"periodEnd":input["periodEnd"]})

            val = tmp.values()
            prod = product(*val)
            for conf in prod:
                # Product maintains the order of values so zip(tmp.keys(),conf) works
                req = MyRequest(art_code,dict(zip(tmp.keys(),conf)),self.res)
                ret.append(req)
        return ret


    def get(self):
        """Execute all requests"""
        for req in self.requests:
            req.get()

    def process(self):
        """Process API responses"""
        for req in self.requests:
            req.process()

    def save_csv(self):
        """Save all requests to csv"""
        for req in self.requests:
            req.save_csv()

    def save_pkl(self):
        """Save all requests to pkl"""
        for req in self.requests:
            req.save_pkl()




