import sys
import json
import os

if __name__ == "__main__":
    path = os.path.join(os.path.dirname(os.path.dirname(os.path.realpath(__file__))),"shared_resources")
    with open(os.path.join(path,"articles.json"),'r') as in_file:
        raw = json.load(in_file)

    out_dict = {}
    for key in raw.keys():
        out_dict.update({raw[key]["name"]:key})

    with open(os.path.join(path,"article_to_code.json"),'w') as out_file:
        json.dump(out_dict, out_file, indent=2)
    