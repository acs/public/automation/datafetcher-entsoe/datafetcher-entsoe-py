# DataFetcher

Automated Fetcher for Documents released at
[transparency.entsoe.eu](https://transparency.entsoe.eu/content/static_content/Static%20content/sitemap/Sitemap-pub.html)

This code uses the [provided API](https://transparency.entsoe.eu/content/static_content/download?path=/Static%20content/web%20api/RestfulAPI_IG.pdf)

[This guide](https://transparency.entsoe.eu/content/static_content/Static%20content/web%20api/Guide.html) may provide further information regarding the API.

## Structure

The project in split into two separate implementation one using Java and one using Python as programing language,
this may change latter if compelling arguments to discontinue one implemnteation arise.
Both of them are independant but are developed alongside each other to keep them similar especially in regard to their interface.


## Interface

Since both implementations will ideally have the same CLI it will be documented here.

## More information

Both implementations have their own documentation inside the corresponding folder.
