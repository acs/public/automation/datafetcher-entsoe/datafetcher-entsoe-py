"""Fetch script for documents released at
[transparency.entsoe.eu](https://transparency.entsoe.eu/content/static_content/Static%20content/sitemap/Sitemap-pub.html)

This code uses the [provided API](https://transparency.entsoe.eu/content/static_content/download?path=/Static%20content/web%20api/RestfulAPI_IG.pdf)
[This guide](https://transparency.entsoe.eu/content/static_content/Static%20content/web%20api/Guide.html) may provide further information regarding the API.


Example input: python .\fetch.py -s 201601012300 -e 201701022300 --in_Domain "Czech Republic, CEPS BZ / CA/ MBA" -a 6.1.a 6.1.b 14.1.a 16.1.bc --outBiddingZone_Domain 10YCZ-CEPS-----N --documentType A75
"""
from Source.Fetcher import Fetcher

f = Fetcher()
f.get()
f.process()
f.save_csv()
f.save_pkl()